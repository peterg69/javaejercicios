package ejercicios;

public class Sphere {
	
	private Circle circ;
	
	public Sphere(Circle c){
		circ = c;
	}
	
	public double calculateArea(){
		return circ.getRadius() * circ.getRadius() * 4 * 3.14159;
	}

	public String toString(){
		return "Sphere Characteristics"
				+ "\nSide: " + circ.getRadius()
				+ "\nDiameter: " + circ.calculateDiameter()
				+ "\nCircumference: " + circ.calculateCircumference()
				+ "\nArea: " + calculateArea();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Circle c = new Circle();
		c.setRadius(25.55);
		Sphere s = new Sphere(c);
		System.out.println(c);
		System.out.println();
		System.out.println();
		System.out.println(s);
		
	}

}
