package ejercicios;

import java.util.Scanner;
import java.util.Hashtable;

public class Person {
	private long dpi;
	private String fname;
	private String lname;
	private int age;
	private String status;
	
	public long getDPI(){
		return dpi;
	}
	
	public String getFirstName(){
		return fname;
	}
	
	public String getLastName(){
		return lname;
	}
	
	public int getAge(){
		return age;
	}
	
	public String getStatus(){
		return status;
	}
	
	public void setDPI(long dpi){
		this.dpi = dpi;
	}
	
	public void setFirstName(String fname){
		this.fname = fname;
	}
	
	public void setLastName(String lname){
		this.lname = lname;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
	public void setStatus(String status){
		this.status = status;
	}
	
	public static void main(String[] args) {

		Hashtable<Long, Person> tab = new Hashtable<Long, Person>();
		Scanner sc = new Scanner(System.in);
		boolean b = true;
		System.out.println("---Menu---"
				+ "\nType option number and press ENTER\n"
				+ "\n1. Register a person"
				+ "\n2. Show the names"
				+ "\n3. Show all the ages"
				+ "\n4. Show all the information of a specific person"
				+ "\n5. Exit");
		while(b){
		
		
		String op = sc.nextLine();
		if(op.equals("1")){
			
			Person p = new Person();
			System.out.println("Enter your DPI: ");
			p.setDPI(Long.parseLong(sc.next()));
			System.out.println("Enter your first name: ");
			p.setFirstName(sc.next());
			System.out.println("Enter your last name: ");
			p.setLastName(sc.next());
			System.out.println("Enter your age: ");
			p.setAge(Integer.parseInt(sc.next()));
			System.out.println("Do you have a job? (yes/no): ");
			p.setStatus(sc.next());
			tab.put(p.getDPI(), p);
			
		} else if(op.equals("2")){
			tab.forEach((k,v)->{
				System.out.println(k +": "+ v.getFirstName() +" "+v.getLastName());
			});
		} else if(op.equals("3")){
			tab.forEach((k,v)->{
				System.out.println(k +": "+ v.getAge());
			});
		} else if(op.equals("4")){
			tab.forEach((k,v)->{
				System.out.println(k +": "+ v.getFirstName() +" "+v.getLastName()+" "+ v.getAge()+" "+v.getStatus());
			});
		} else if(op.equals("5")){
			System.out.println("Goodbye");
			sc.close();
			b = false;
		} else {
			System.out.println("---Menu---"
					+ "\nType option number and press ENTER\n"
					+ "\n1. Register a person"
					+ "\n2. Show the names"
					+ "\n3. Show all the ages"
					+ "\n4. Show all the information of a specific person"
					+ "\n5. Exit");
		}
		}
	}

}
