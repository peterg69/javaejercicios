package ejercicios;

import java.util.Random; 

public class Ej1_thread extends Thread  {

	public static void main(String[] args) throws InterruptedException {
		Random r = new Random();
		for(int i=0; i<10; i++){
			int num = r.nextInt(100) + 1;
			System.out.print(num);
			if(num%2 == 0)
				System.out.println(" yes");
			else
				System.out.println(" no");
			Thread.sleep(1000);
		}
		
	}

}