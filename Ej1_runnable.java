package ejercicios;

import java.util.Random; 

public class Ej1_runnable implements Runnable  {
	public void run(){
		Random r = new Random();
		for(int i=0; i<10; i++){
			int num = r.nextInt(100) + 1;
			System.out.print(num);
			if(num%2 == 0)
				System.out.println(" yes");
			else
				System.out.println(" no");
			
			try{
				Thread.sleep(1000);
	        } catch (InterruptedException iex) {
	            System.out.println("Exception in thread: " + iex.getMessage());
	        }
		}
    }

	public static void main(String[] args) throws InterruptedException {
		
		Ej1_runnable p = new Ej1_runnable();
	    new Thread(p).start();
	     
	}

}
